CXX = g++
LIBS_NAME =
INCLUDES = $(shell pkg-config --cflags $(LIBS_NAME) 2> /dev/null)
LDLIBS = $(shell pkg-config --libs $(LIBS_NAME) 2> /dev/null)
WARNS = -Wall -Wextra -Weffc++ -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Wlogical-op -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wconversion -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel
CXXFLAGS = -std=c++20 $(WARNS) $(INCLUDES)\
           $(if $(DEBUG),-O0 -ggdb,-march=native -O3 -DNDEBUG)\
           $(if $(PROFILER),-DPROFILER)

EXEC = generate process test
all: $(EXEC)

generate: src/generate.cpp src/Point.cpp src/Point.hpp
	$(CXX) $(CXXFLAGS) src/generate_scu.cpp -o $@ $(LDLIBS)

# Must be in the same compilation unit for __COUNTER__ to work
process: src/process.cpp src/Point.cpp src/jsonparser.cpp src/ibp.cpp \
         src/Point.hpp src/jsonparser.hpp src/ibp.hpp
	$(CXX) $(CXXFLAGS) src/process_scu.cpp -o $@ $(LDLIBS)

test: src/test.cpp src/ibp.cpp
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LDLIBS)

clean:
	rm -rf $(EXEC)
