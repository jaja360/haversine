#include "Point.hpp"
#include <cmath>
#include <sstream>

using namespace std;
constexpr double EarthRadius = 6372.8;

Point::Point(double _x, double _y) : x(_x), y(_y) {}

auto square(const auto& a) {
  return a * a;
}

auto degToRad(const auto& deg) {
  constexpr auto convFactor = 2.0 * M_PI / 360.0;
  return convFactor * deg;
}

string pairToJson(const Point& p1, const Point& p2) {
  stringstream ss;
  ss.precision(numeric_limits<double>::max_digits10);
  ss << fixed << "{ "
     << "\"x0\" : " << p1.x << " , "
     << "\"y0\" : " << p1.y << " , "
     << "\"x1\" : " << p2.x << " , "
     << "\"y1\" : " << p2.y << " }";
  return ss.str();
}

double haversineDist(const Point& p1, const Point& p2) {
  const auto phi1    = degToRad(p1.y);
  const auto phi2    = degToRad(p2.y);
  const auto lambda1 = degToRad(p1.x);
  const auto lambda2 = degToRad(p2.x);

  const auto fstSin2 = square(sin((phi2 - phi1) / 2.0));
  const auto sndSin2 = square(sin((lambda2 - lambda1) / 2.0));
  const auto bigParen = sqrt(fstSin2 + cos(phi1) * cos(phi2) * sndSin2);
  return 2.0 * EarthRadius * asin(bigParen);
}
