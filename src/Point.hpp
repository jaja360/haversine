#ifndef POINT_HPP
#define POINT_HPP

#include <string>

class Point {
 public:
  Point(double x, double y);

  friend std::string pairToJson(const Point& p1, const Point& p2);
  friend double haversineDist(const Point& p1, const Point& p2);

 private:
  double x;
  double y;
};

#endif
