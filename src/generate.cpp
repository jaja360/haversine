#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>

#include "Point.hpp"

using namespace std;
using Distrib = uniform_real_distribution<double>;

enum Mode {
  uniform,
  cluster,
};
const array<string, 2> ModeStr = {"uniform", "cluster"};

pair<Point, Point> generatePair(Distrib& lngdist, Distrib& latdist,
                                mt19937& gen) {
  const Point p1(lngdist(gen), latdist(gen));
  const Point p2(lngdist(gen), latdist(gen));
  return {p1, p2};
};

pair<Distrib, Distrib> generateClusterDistrib(Distrib& lngdist,
                                              Distrib& latdist, mt19937& gen) {
  double minlng = lngdist(gen);
  double maxlng = lngdist(gen);
  if (maxlng < minlng) swap(minlng, maxlng);

  double minlat = latdist(gen);
  double maxlat = latdist(gen);
  if (maxlat < minlat) swap(minlat, maxlat);

  Distrib lngdistClust(minlng, maxlng);
  Distrib latdistClust(minlat, maxlat);
  return {lngdistClust, latdistClust};
}

double generateJsonPoints(Mode mode, size_t seed, size_t npairs) {
  stringstream ssjson, ssbin;
  ssjson << "data_" << npairs << "_" << ModeStr[mode] << ".json";
  ssbin  << "data_" << npairs << "_" << ModeStr[mode] << ".f64";
  fstream filejson(ssjson.str(), ios::out);
  fstream filebin(ssbin.str(), ios::out | ios::binary);
  if (!filejson.is_open() || !filebin.is_open()) {
    cerr << "Failed to open the output files: "
         << ssjson.str() << " and " << ssbin.str() << endl;
    exit(EXIT_FAILURE);
  }

  mt19937 gen(seed);
  double sumDist = 0.0;
  filejson << "{\n"
           << "  \"pairs\" : [\n";

  auto processPair = [&](Distrib& latdist, Distrib& lngdist) {
    const auto& [p1, p2] = generatePair(lngdist, latdist, gen);
    const double distance = haversineDist(p1, p2);
    sumDist += distance;
    filebin.write(reinterpret_cast<const char*>(&distance), sizeof(double));
    filejson << "    " << pairToJson(p1, p2) << ",\n";
  };

  Distrib lngdist(-180.0, 180.0);
  Distrib latdist(-90.0, 90.0);
  if (mode == uniform) {
    for (size_t i = 0; i < npairs; ++i)
      processPair(latdist, lngdist);
  } else if (mode == cluster) {
    constexpr size_t NumPairsPerCluster = 64;
    for (size_t i = 0; i < npairs;) {
      auto [lngdistClust, latdistClust] =
          generateClusterDistrib(lngdist, latdist, gen);
      for (size_t j = 0; j < NumPairsPerCluster && i < npairs; ++i, ++j)
        processPair(latdistClust, lngdistClust);
    }
  } else
    assert(false);

  const double avgDist = sumDist / static_cast<double>(npairs);
  filejson.seekp(-2, std::ios_base::end);
  filejson << "\n"
           << "  ]\n"
           << "}" << endl;
  filebin.write(reinterpret_cast<const char*>(&avgDist), sizeof(double));

  return avgDist;
}

int main(int argc, char* argv[]) {
  if (argc != 4) {
    cerr << "Usage: ./generate (uniform|cluster) <seed> <num_pairs>" << endl;
    return EXIT_FAILURE;
  }

  Mode mode;
  if (string(argv[1]) == "uniform")
    mode = uniform;
  else if (string(argv[1]) == "cluster")
    mode = cluster;
  else {
    cerr << "Invalid generation mode: " << argv[1] << endl;
    return EXIT_FAILURE;
  }

  const size_t seed = strtoul(argv[2], nullptr, 10);
  const size_t npairs = strtoul(argv[3], nullptr, 10);

  const double avgDist = generateJsonPoints(mode, seed, npairs);
  cout << "Method: " << argv[1]
       << "\nRandom seed: " << seed
       << "\nPair count: " << npairs
       << "\nExpected sum: " << avgDist << endl;

  return EXIT_SUCCESS;
}
