#include "ibp.hpp"

#include <asm/unistd.h>
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>
#include <x86intrin.h>

#include <cstring>
#include <format>
#include <iomanip>
#include <iostream>
#include <limits>

using namespace std;

uint64_t ibp::getOSTimerFreq() {
  return 1'000'000ul;  // on POSIX system, it's always 1 Mhz (i.e., 1 µs)
}

uint64_t ibp::getOSTimerValue() {
  timeval tv;
  gettimeofday(&tv, 0);
  return (getOSTimerFreq() * uint64_t(tv.tv_sec)) + uint64_t(tv.tv_usec);
}

uint64_t ibp::getCPUTimer() {
  return __rdtsc();
}

uint64_t ibp::getCPUFreq(uint64_t msToWait) {
  const uint64_t OSFreq = getOSTimerFreq();
  const uint64_t OSWaitTime = OSFreq * msToWait / 1000ul;

  const uint64_t OSStart = getOSTimerValue();
  const uint64_t CPUStart = getCPUTimer();

  uint64_t OSEnd = 0ul;
  uint64_t OSElapsed = 0ul;
  while (OSElapsed < OSWaitTime) {
    OSEnd = getOSTimerValue();
    OSElapsed = OSEnd - OSStart;
  }

  const uint64_t CPUEnd = getCPUTimer();
  const uint64_t CPUElapsed = CPUEnd - CPUStart;
  const uint64_t CPUFreq = OSFreq * CPUElapsed / OSElapsed;
  return CPUFreq;
}

double ibp::secondsFromFreq(uint64_t clocks) {
  static const uint64_t CPUFreq = getCPUFreq();
  return double(clocks) / double(CPUFreq);
}

ibp::DecomposedVirtAddr ibp::getVirtAddrDecomp(void* p, ibp::PageSize ps) {
  ibp::DecomposedVirtAddr decomp{0, 0, 0, 0, 0};

  uintptr_t pi = reinterpret_cast<uintptr_t>(p);
  decomp.PML4Index = ((pi >> 39) & 0x1ff);
  decomp.DirectoryPtrIndex = ((pi >> 30) & 0x1ff);

  if (ps == ibp::_4k) {
    decomp.DirectoryIndex = ((pi >> 21) & 0x1ff);
    decomp.TableIndex = ((pi >> 12) & 0x1ff);
    decomp.Offset = ((pi >> 0) & 0xfff);
  } else if (ps == ibp::_2M) {
    decomp.DirectoryIndex = ((pi >> 21) & 0x1ff);
    decomp.Offset = ((pi >> 0) & 0x1fffff);
  } else {  // ps == ibp::_1G
    decomp.Offset = ((pi >> 0) & 0x3fffffff);
  }

  return decomp;
}

void ibp::printVirtAddrDecomp(const ibp::DecomposedVirtAddr& decomp) {
  printf("|%3u|%3u|%3u|%3u|%10u|", decomp.PML4Index, decomp.DirectoryPtrIndex,
         decomp.DirectoryIndex, decomp.TableIndex, decomp.Offset);
}

long ibp::perf_event_open(perf_event_attr* hw_event, pid_t pid, int cpu,
                          int group_fd, unsigned long flags) {
  return syscall(__NR_perf_event_open, hw_event, pid, cpu, group_fd, flags);
}

int ibp::getPerfEventFd(uint64_t perfEvent, uint32_t perfType) {
  perf_event_attr pe_attr;
  memset(&pe_attr, 0, sizeof(pe_attr));
  pe_attr.size = sizeof(pe_attr);
  pe_attr.type = perfType;
  pe_attr.config = perfEvent;
  pe_attr.disabled = 1;
  pe_attr.exclude_kernel = 1;
  int fd = static_cast<int>(perf_event_open(&pe_attr));
  if (fd == -1) {
    cerr << "perf_event_open failed: " << strerror(errno) << endl;
    exit(EXIT_FAILURE);
  }
  return fd;
}

uint64_t ibp::getPageFaultsPerfEvent(bool reset) {
  static int fd = getPerfEventFd(PERF_COUNT_SW_PAGE_FAULTS);
  if (reset) {
    ioctl(fd, PERF_EVENT_IOC_RESET, 0);
    ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);
    return 0;
  }

  ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);
  uint64_t pfCount;
  if (read(fd, &pfCount, sizeof(pfCount)) == -1) {
    cerr << "getPageFaults: " << strerror(errno) << endl;
    exit(EXIT_FAILURE);
  }
  return pfCount;
}

uint64_t ibp::getPageFaultsRUsage() {
  rusage ru;
  getrusage(RUSAGE_SELF, &ru);
  return static_cast<uint64_t>(ru.ru_majflt + ru.ru_minflt);
}

void ibp::comparisonTester(vector<pair<string, function<void(void)>>> tests,
                           uint64_t byteCount, uint64_t numItems,
                           uint64_t secondsToTry,
                           function<void(void)> initBetweenTests) {
  vector<RepMetrics> metrics;
  for (const auto& [label, f] : tests) {
    initBetweenTests();
    const auto m = repetitionTester(label.c_str(), f, byteCount, secondsToTry);
    metrics.push_back(m);
  }

  cout << format("\n{:>12s} {:>12s} {:>12s} {:>12s} {:>12s} {:>12s} {:>13s}\n",
                 "Label", "NumItems", "ByteCount", "AvgCycles", "GB/s",
                 "Cycles/item", "Speedup");
  const uint64_t avgBaseline = metrics[0].avgClocks;
  const double processedGB = double(byteCount) / (1024.0 * 1024.0 * 1024.0);
  for (size_t i = 0; i < size(tests); ++i) {
    const auto& [label, _] = tests[i];
    const auto& m = metrics[i];
    cout << format("{:>12s} {:12} {:12} {:12} {:12.2f} {:12.2f} {:12.2f}x\n",
                   label, numItems, byteCount, m.avgClocks,
                   double(processedGB) / secondsFromFreq(m.avgClocks),
                   double(m.avgClocks) / double(numItems),
                   double(avgBaseline) / double(m.avgClocks));
  }
}

ibp::RepMetrics ibp::repetitionTester(const char* label, function<void(void)> f,
                                      uint64_t byteCount,
                                      uint64_t secondsToTry) {
  const uint64_t clocksToWait = getCPUFreq() * secondsToTry;
  const uint64_t CPUStartGlobal = getCPUTimer();
  uint64_t CPUStartSinceReset = CPUStartGlobal;

  uint64_t minClocks = numeric_limits<uint64_t>::max();
  uint64_t maxClocks = 0;
  uint64_t minPF = numeric_limits<uint64_t>::max();
  uint64_t maxPF = 0;
  uint64_t totalPF = 0;

  uint32_t numRepetitions = 0;
  while (getCPUTimer() - CPUStartSinceReset < clocksToWait) {
    ++numRepetitions;

    const uint64_t PFStart = getPageFaultsRUsage();
    const uint64_t CPUStartScope = getCPUTimer();
    f();
    const uint64_t durationClocks = getCPUTimer() - CPUStartScope;
    const uint64_t PF = getPageFaultsRUsage() - PFStart;

    if (durationClocks < minClocks) {
      minClocks = durationClocks;
      CPUStartSinceReset = getCPUTimer();
    }
    maxClocks = max(maxClocks, durationClocks);
    totalPF += PF;
    minPF = min(minPF, PF);
    maxPF = max(maxPF, PF);
  }

  const uint64_t avgClocks = (getCPUTimer() - CPUStartGlobal) / numRepetitions;
  const uint64_t avgPF = totalPF / numRepetitions;

  const auto printSuffix = [byteCount](const char* name, uint64_t clocks,
                                       uint64_t PF) {
    const double processedGB = double(byteCount) / (1024.0 * 1024.0 * 1024.0);
    const double time = secondsFromFreq(clocks);
    stringstream ss, ssPF;
    if (PF != 0)
      ssPF << PF << " (" << double(byteCount) / double(PF) << " B/faults)";
    else
      ssPF << "0";
    ss << name << ": " << clocks << " (" << time << "ms)"
       << " " << processedGB / time << "GiB/s"
       << " PF: " << ssPF.str();
    return ss.str();
  };

  cout << "\n--- " << label << " (" << numRepetitions << " repetitions) ---\n"
       << printSuffix("Min", minClocks, minPF) << "\n"
       << printSuffix("Max", maxClocks, maxPF) << "\n"
       << printSuffix("Avg", avgClocks, avgPF) << "\n"
       << endl;

  return {minClocks, maxClocks, avgClocks};
}

static ibp::GlobalProfiler gp;

void ibp::BeginProfiler() {
  gp.globalBeginTSC = getCPUTimer();
}

#if PROFILER

static size_t GlobalParentIndex;

ibp::ScopeProfiler::ScopeProfiler(const char* label, size_t index,
                                  uint64_t byteCount)
    : scopeData(gp.data[index]),
      startScopeTSC(getCPUTimer()),
      oldTSCInclusive(scopeData.TSCElapsedInclusive),
      parentIndex(GlobalParentIndex) {
  GlobalParentIndex = index;
  scopeData.label = label;
  scopeData.processedByteCount += byteCount;
}

ibp::ScopeProfiler::~ScopeProfiler() {
  const uint64_t elapsed = getCPUTimer() - startScopeTSC;

  scopeData.TSCElapsedExclusive += elapsed;
  scopeData.TSCElapsedInclusive = oldTSCInclusive + elapsed;
  ++scopeData.hitCount;

  gp.data[parentIndex].TSCElapsedExclusive -= elapsed;
  GlobalParentIndex = parentIndex;
}

void ibp::printElapsed(const ScopeData& data, uint64_t totalTSC) {
  const double percentExclusive =
      100.0 * double(data.TSCElapsedExclusive) / double(totalTSC);
  cout << "  " << setw(10) << left << data.label << "\t[" << setw(8) << right
       << data.hitCount << "]"
       << "\t" << setw(10) << data.TSCElapsedExclusive << "\t(" << fixed
       << setprecision(2) << percentExclusive << "%";

  if (data.TSCElapsedExclusive != data.TSCElapsedInclusive) {
    const double percentInclusive =
        100.0 * double(data.TSCElapsedInclusive) / double(totalTSC);
    cout << ", " << percentInclusive << "% w/children";
  }
  cout << ")";

  if (data.processedByteCount != 0) {
    constexpr double Mebibyte = 1024.0 * 1024.0;
    constexpr double Gibibyte = 1024.0 * Mebibyte;
    const double elapsedSec = secondsFromFreq(data.TSCElapsedInclusive);
    const double mebibytes = double(data.processedByteCount) / Mebibyte;
    const double bytesPerSecond = double(data.processedByteCount) / elapsedSec;
    const double gigabytesPerSecond = bytesPerSecond / Gibibyte;
    cout << "\t" << mebibytes << "MiB at " << gigabytesPerSecond << "GiB/s";
  }

  cout << "\n";
}

#endif

void ibp::StopAndPrintProfiler() {
  const uint64_t totalCPUTSC = getCPUTimer() - gp.globalBeginTSC;
  const double totalTimeMS = 1000.0 * secondsFromFreq(totalCPUTSC);
  cout << "Total time: " << fixed << setprecision(1) << totalTimeMS << "ms"
       << " (TSC: " << totalCPUTSC << ", CPU freq: " << getCPUFreq() << ")\n";

#if PROFILER
  for (size_t i = 1; i < size(gp.data); ++i)
    if (gp.data[i].hitCount != 0) printElapsed(gp.data[i], totalCPUTSC);
#endif
}
