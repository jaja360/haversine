#ifndef IBP_HPP
#define IBP_HPP

#include <linux/perf_event.h>

#include <cstdint>
#include <functional>
#include <string>

namespace ibp {

constexpr uint64_t TimeCPUClockEstimationMS = 100;

uint64_t getOSTimerFreq();
uint64_t getOSTimerValue();
uint64_t getCPUTimer();
uint64_t getCPUFreq(uint64_t msToWait = TimeCPUClockEstimationMS);
double secondsFromFreq(uint64_t clocks);

long perf_event_open(perf_event_attr* hw_event, pid_t pid = 0, int cpu = -1,
                     int group_fd = -1, unsigned long flags = 0);
int getPerfEventFd(uint64_t perfEvent, uint32_t perfType = PERF_TYPE_SOFTWARE);

/**
 * Returns the decomposition of a pointer.
 * https://wiki.osdev.org/Page_Tables#4_KiB_pages_3
 *
 * E.g., for 4k pages: [16 bits unused,
 *                      PML4: 9 bits,
 *                      PDP : 9 bits,
 *                      PD  : 9 bits,
 *                      PT  : 9 bits,
 *                      12 bits offset]
 */
struct DecomposedVirtAddr {
  uint16_t PML4Index;
  uint16_t DirectoryPtrIndex;
  uint16_t DirectoryIndex;
  uint16_t TableIndex;
  uint32_t Offset;
};
enum PageSize { _4k, _2M, _1G };
DecomposedVirtAddr getVirtAddrDecomp(void* p, PageSize ps = _4k);
void printVirtAddrDecomp(const DecomposedVirtAddr& decomp);

uint64_t getPageFaultsPerfEvent(bool reset = false);
uint64_t getPageFaultsRUsage();

void BeginProfiler();
void StopAndPrintProfiler();

struct RepMetrics {
  uint64_t minClocks;
  uint64_t maxClocks;
  uint64_t avgClocks;
};
void comparisonTester(
    std::vector<std::pair<std::string, std::function<void(void)>>>,
    uint64_t byteCounts, uint64_t numItems = 1, uint64_t secondsToTry = 10,
    std::function<void(void)> initBetweenTests = {});
RepMetrics repetitionTester(const char* label, std::function<void(void)> f,
                            uint64_t byteCount = 0, uint64_t secondsToTry = 10);

#if PROFILER

constexpr uint64_t NumMaxProfilableScopes = 64;

struct ScopeData {
  char const* label;
  uint64_t TSCElapsedExclusive;  // does not include children
  uint64_t TSCElapsedInclusive;  // does include children
  uint64_t hitCount;
  uint64_t processedByteCount;
};

struct GlobalProfiler {
  std::array<ScopeData, NumMaxProfilableScopes> data{};
  uint64_t globalBeginTSC{};
};

class ScopeProfiler {
 public:
  ScopeProfiler(const char* label, size_t index, uint64_t byteCount = 0);
  ~ScopeProfiler();

  // Prevent -Weffc++ warning
  ScopeProfiler(const ScopeProfiler&) = delete;
  ScopeProfiler& operator=(const ScopeProfiler&) = delete;

 private:
  ScopeData& scopeData;
  uint64_t startScopeTSC;
  uint64_t oldTSCInclusive;
  size_t parentIndex;
};

void printElapsed(const ScopeData& data, uint64_t totalTSC);

#define NameConcat2(A, B) A##B
#define NameConcat(A, B) NameConcat2(A, B)

#define TimeScopeBandwidth(Name, ByteCount)                             \
  ibp::ScopeProfiler NameConcat(Scope, __LINE__)(Name, __COUNTER__ + 1, \
                                                 ByteCount)
#define TimeScope(Name) TimeScopeBandwidth(Name, 0)
#define TimeFunction() TimeScope(__func__)
#define TimeFunctionBandwidth(ByteCount) TimeScopeBandwidth(__func__, ByteCount)
#define ProfilerEndOfCU()                                  \
  static_assert(__COUNTER__ < ibp::NumMaxProfilableScopes, \
                "Number of profile points exceeds max")

#else

#define TimeScopeBandwidth(...)
#define TimeScope(...)
#define TimeFunctionBandwidth(...)
#define TimeFunction(...)
#define ProfilerEndOfCU(...)

struct GlobalProfiler {
  uint64_t globalBeginTSC{};
};

#endif  // PROFILER

}  // namespace ibp

#endif
