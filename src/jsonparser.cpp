#include "jsonparser.hpp"

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>

#include "ibp.hpp"

using namespace std;

json::Parser::Parser(const string& filename) {
  int fd = open(filename.c_str(), O_RDONLY | O_LARGEFILE);
  if (fd == -1) {
    cerr << "The JSON file (" << filename << ") could not be opened:\n"
         << "\t" << strerror(errno) << endl;
    exit(EXIT_FAILURE);
  }

  struct stat sb;
  fstat(fd, &sb);
  const size_t fs = static_cast<size_t>(sb.st_size);

  constexpr int flags = MAP_ANONYMOUS | MAP_PRIVATE;
  constexpr size_t _2mb = 2 * 1024 * 1024;
  const size_t rs = (fs + _2mb - 1) & -_2mb;
  void* mem = mmap(nullptr, rs, PROT_READ | PROT_WRITE, flags, -1, 0);

  if (mem == MAP_FAILED) {
    perror("mmap()");
    exit(EXIT_FAILURE);
  }

  ssize_t rdsize;
  {
    TimeScopeBandwidth("Read File", fs);
    rdsize = read(fd, mem, fs);
  }

  if(rdsize == -1) {
    perror("read()");
    exit(EXIT_FAILURE);
  }

  memblock = static_cast<char*>(mem);
  parseRoot();
}

const json::JObject& json::Parser::getRoot() const {
  return root;
}

void json::Parser::skipWhiteSpaces() {
  do {
    readc();
  } while (isspace(peek));
}

void json::Parser::findNextToken() {
  skipWhiteSpaces();
  switch (peek) {
    case '{':
      tokenType = CURLY_OPEN;
      break;
    case '}':
      tokenType = CURLY_CLOSE;
      break;
    case '[':
      tokenType = ARRAY_OPEN;
      break;
    case ']':
      tokenType = ARRAY_CLOSE;
      break;
    case ':':
      tokenType = COLON;
      break;
    case ',':
      tokenType = COMMA;
      break;

    case 'f':
      tokenType = parseKeyword("false") ? BOOL_FALSE : ERROR;
      break;
    case 't':
      tokenType = parseKeyword("true") ? BOOL_TRUE : ERROR;
      break;
    case 'n':
      tokenType = parseKeyword("null") ? NULL_TYPE : ERROR;
      break;

    case '"':
      tokenType = STRING;
      parseString();
      break;

    case '-':
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      tokenType = NUMBER;
      parseNumber();
      break;

    default:
      tokenType = ERROR;
  }
}

void json::Parser::readc() {
  peek = memblock[pos++];
}

bool json::Parser::parseKeyword(const string& keyword) {
  for (const char c : keyword) {
    if (c != peek) return false;
    readc();
  }
  tokenStr = keyword;
  return true;
}

void json::Parser::parseString() {
  tokenStr = "";
  while (true) {
    readc();
    if (peek == '"') return;
    tokenStr += peek;
  }
}

void json::Parser::parseNumber() {
  const bool negative = (peek == '-');
  if (negative) readc();

  unsigned long v = 0;
  int nbDecimals = 0;
  bool decimal = false;

  do {
    if (decimal) ++nbDecimals;
    if (peek == '.')
      decimal = true;
    else
      v = 10 * v + static_cast<unsigned long>(peek - '0');
    readc();
  } while (isdigit(peek) || peek == '.');

  tokenNum = static_cast<double>(v);
  if (nbDecimals > 0) tokenNum /= pow(10, nbDecimals);
  if (negative) tokenNum *= -1;
}

void json::Parser::parseRoot() {
  TimeFunction();

  findNextToken();
  root = get<JObject>(parseElement().v);
}

json::JValue json::Parser::parseElement() {
  TimeFunction();

  switch (tokenType) {
    case STRING: return tokenStr;
    case NUMBER: return tokenNum;
    case BOOL_TRUE: return true;
    case BOOL_FALSE: return false;
    case NULL_TYPE: return nullptr;

    case CURLY_OPEN: return parseObject();
    case ARRAY_OPEN: return parseArray();

    case CURLY_CLOSE:
    case ARRAY_CLOSE:
    case COMMA:
    case COLON:
    case ERROR:
    default:
      cerr << "Invalid token" << endl;
      exit(EXIT_FAILURE);
  }
}

json::JObject json::Parser::parseObject() {
  TimeFunction();

  JObject currObject;

  while (true) {
    findNextToken();
    if (tokenType == CURLY_CLOSE) break;
    const JValue keyElem = parseElement();
    const string& key = get<string>(keyElem.v);

    findNextToken();
    assert(tokenType == COLON);

    findNextToken();
    const JValue value = parseElement();
    currObject.emplace(key, value);

    findNextToken();
    if (tokenType == CURLY_CLOSE) break;
    assert(tokenType == COMMA);
  }

  return currObject;
}

json::JArray json::Parser::parseArray() {
  TimeFunction();

  JArray currArray;

  while (true) {
    findNextToken();
    if (tokenType == ARRAY_CLOSE) break;
    currArray.push_back(parseElement());

    findNextToken();
    if (tokenType == ARRAY_CLOSE) break;
    assert(tokenType == COMMA);
  }

  return currArray;
}
