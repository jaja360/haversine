#ifndef JSONPARSER_HPP
#define JSONPARSER_HPP

#include <fstream>
#include <map>
#include <numeric>
#include <string>
#include <variant>
#include <vector>

namespace json {

struct JValue;
using JArray = std::vector<JValue>;
using JObject = std::map<std::string, JValue>;

struct JValue {
  JValue(const auto& value) : v(value) {}
  std::variant<JObject, std::string, JArray, double, nullptr_t, bool> v;
};

class Parser {
 public:
  Parser() = default;
  Parser(const std::string& filename);

  // No copy
  Parser(const Parser&) = delete;
  Parser& operator=(const Parser&) = delete;

  // Move ok
  Parser(Parser&&) = default;
  Parser& operator=(Parser&&) = default;

  const JObject& getRoot() const;
 private:
  enum TokenType {
    CURLY_OPEN,
    CURLY_CLOSE,
    ARRAY_OPEN,
    ARRAY_CLOSE,
    COLON,
    COMMA,
    STRING,
    NUMBER,
    BOOL_TRUE,
    BOOL_FALSE,
    NULL_TYPE,
    ERROR,
  };

  void skipWhiteSpaces();
  void findNextToken();
  void readc();
  bool parseKeyword(const std::string& keyword);
  void parseString();
  void parseNumber();
  void parseRoot();
  JValue parseElement();
  JObject parseObject();
  JArray parseArray();

  JObject root{};

  // For the Lexer
  char* memblock = nullptr;
  int pos = 0;
  char peek = ' ';
  TokenType tokenType = ERROR;
  std::string tokenStr = "";
  double tokenNum = std::numeric_limits<double>::infinity();
};

}  // namespace json

#endif
