#include <fstream>
#include <iostream>

#include "Point.hpp"
#include "ibp.hpp"
#include "jsonparser.hpp"

using namespace std;
using namespace ibp;

pair<double, size_t> avgJSONPairs(const json::JObject& root) {
  const auto& pairsObj = root.at("pairs");
  const auto& pairs = get<json::JArray>(pairsObj.v);
  const size_t nPairs = size(pairs);

  TimeFunctionBandwidth(nPairs * 2 * sizeof(Point));
  double sumDist = 0.0;
  for (size_t i = 0; i < nPairs; ++i) {
    const auto& pairPoints = get<json::JObject>(pairs[i].v);
    const Point& p1 = Point(get<double>(pairPoints.at("x0").v),
                            get<double>(pairPoints.at("y0").v));
    const Point& p2 = Point(get<double>(pairPoints.at("x1").v),
                            get<double>(pairPoints.at("y1").v));
    sumDist += haversineDist(p1, p2);
  }

  const double avgDist = sumDist / static_cast<double>(nPairs);
  return {avgDist, nPairs};
}

int main(int argc, char* argv[]) {
  ibp::BeginProfiler();

  if (argc <= 1 || argc >= 4) {
    cerr << "Usage: ./processor <json_file> [answer.f64]" << endl;
    return EXIT_FAILURE;
  }

  const string filename_json(argv[1]);
  const json::Parser parser = json::Parser(filename_json);
  const auto& jsonRoot = parser.getRoot();

  const auto [avgDist, nPairs] = avgJSONPairs(jsonRoot);

  cout.precision(numeric_limits<double>::digits10);
  cout << "Pair count: " << nPairs << "\nComputed sum: " << avgDist << endl;

  if (argc == 3) {
    TimeScopeBandwidth("validate", sizeof(double) * (nPairs + 1ul));

    const string filename_answer(argv[2]);
    ifstream ifs(filename_answer, ios::binary);
    if (!ifs.is_open()) {
      cerr << "The answer file (" << filename_answer << ") could not be opened"
           << endl;
      return EXIT_FAILURE;
    }

    vector<double> answers(nPairs + 1ul);
    ifs.read(reinterpret_cast<char*>(answers.data()),
             static_cast<streamsize>((nPairs + 1ul) * sizeof(double)));
    const double answer = answers.back();
    cout << "\nValidation:"
         << "\n  Reference sum: " << answer
         << "\n  Difference: " << avgDist - answer << "\n"
         << endl;
  }

  ibp::StopAndPrintProfiler();
  return EXIT_SUCCESS;
}
