#include <iostream>

#include "ibp.hpp"

using namespace std;
using namespace ibp;

constexpr uint64_t NumElems = 1024 * 1024 * 1024;

void writeToArrayReuse(uint64_t* t) {
  for (uint64_t i = 0; i < NumElems; ++i) t[i] = i;
}

void writeToArrayNew() {
  volatile uint64_t* t = new uint64_t[NumElems];
  for (uint64_t i = 0; i < NumElems; ++i) t[i] = i;
  delete[] t;
}

int main() {
  uint64_t* t = new uint64_t[NumElems];
  repetitionTester(
      "writeToArray_reuseBuffer", [&]() noexcept { writeToArrayReuse(t); },
      NumElems * sizeof(uint64_t), 3);
  delete[] t;

  repetitionTester(
      "writeToArray_newBuffer", [&]() noexcept { writeToArrayNew(); },
      NumElems * sizeof(uint64_t), 3);

  /* for (uint64_t i = 0; i < NumElems; ++i) { */
  /*   printVirtAddrDecomp(getVirtAddrDecomp(&t[i])); */
  /*   cout << "\n"; */
  /* } */

  return EXIT_SUCCESS;
}
